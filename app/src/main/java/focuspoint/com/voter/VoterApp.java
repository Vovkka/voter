package focuspoint.com.voter;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import focuspoint.com.voter.di.components.AppComponent;
import focuspoint.com.voter.di.components.DaggerAppComponent;
import focuspoint.com.voter.di.modules.AppModule;
import focuspoint.com.voter.di.modules.NetworkModule;

/**
 * Our Application Class.
 * Responsible for the instantiation of the Dagger Graph.
 */


public class VoterApp extends Application {
    AppComponent mComponent;


    @Override
    public void onCreate() {
        super.onCreate();


        mComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public AppComponent getComponent() {
        return mComponent;
    }

    public static VoterApp from (@NonNull Context context){
        return (VoterApp) context.getApplicationContext();
    }
}
