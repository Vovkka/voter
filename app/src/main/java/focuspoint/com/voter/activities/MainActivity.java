package focuspoint.com.voter.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import focuspoint.com.voter.VoterApp;
import focuspoint.com.voter.R;
import focuspoint.com.voter.models.Vote;
import focuspoint.com.voter.network.NetworkService;
import focuspoint.com.voter.presenters.interfaces.IMainPresenter;
import focuspoint.com.voter.views.IMainView;
import retrofit2.Retrofit;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class MainActivity extends AppCompatActivity implements IMainView{

    @Inject Retrofit votesApi;
    @Inject IMainPresenter mainPresenter;


    @BindView(R.id.search_by_name) EditText searchByNameEditText;
    private CompositeSubscription subscriptions;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VoterApp.from(this).getComponent().inject(this);
        unbinder = ButterKnife.bind(this);
        subscriptions = new CompositeSubscription();
        mainPresenter.attachView(this);

        Subscription subscription = RxTextView.textChangeEvents(searchByNameEditText)
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter(textEvent -> textEvent.text().toString().trim().length() > 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(text-> Toast.makeText(this, text.text(), Toast.LENGTH_SHORT).show());
        subscriptions.add(subscription);


        Subscription votesSubscription = votesApi
                .create(NetworkService.class)
                .getVotes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(votes -> {
                    List list = votes;
                }, throwable -> {
                    Toast.makeText(MainActivity.this, throwable.toString(), Toast.LENGTH_SHORT).show();
                });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        subscriptions.clear();
        unbinder.unbind();
        mainPresenter.detachView();
    }
}
