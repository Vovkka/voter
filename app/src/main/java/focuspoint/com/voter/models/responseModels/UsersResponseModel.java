package focuspoint.com.voter.models.responseModels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import focuspoint.com.voter.models.User;

/**
 * User list wrapper
 */


public class UsersResponseModel {
    @SerializedName("response") List<User> userList;


    public List<User> getUserList() {
        return userList;
    }
}
