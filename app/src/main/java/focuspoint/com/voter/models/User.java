package focuspoint.com.voter.models;

import com.google.gson.annotations.SerializedName;

/**
 * User
 */

public class User {
    @SerializedName("uid") private long id;
    @SerializedName("first_name") private String firstName;
    private String lastName;


    @Override
    public String toString() {
        return "id = " + id
                + " name = " + (firstName!= null ? firstName : "");
    }
}
