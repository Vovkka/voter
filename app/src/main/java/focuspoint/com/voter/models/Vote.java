package focuspoint.com.voter.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 11.03.17.
 */

public class Vote {
    @SerializedName("id") long id;
    @SerializedName("description") String description;
}
