package focuspoint.com.voter.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import focuspoint.com.voter.network.NetworkService;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Module for Retrofit instance
 */


@Module
public class NetworkModule {

    @Singleton
    @Provides
    public Retrofit getVotesApi(){
        return new Retrofit.Builder()
                .baseUrl(NetworkService.VOTES_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }
}
