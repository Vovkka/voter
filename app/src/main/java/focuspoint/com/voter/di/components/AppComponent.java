package focuspoint.com.voter.di.components;

import javax.inject.Singleton;

import dagger.Component;
import focuspoint.com.voter.activities.MainActivity;
import focuspoint.com.voter.di.modules.AppModule;
import focuspoint.com.voter.di.modules.NetworkModule;

/**
 * Created by root on 06.03.17.
 */


@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class
})
public interface AppComponent {
    void inject(MainActivity mainActivity);
}