package focuspoint.com.voter.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import focuspoint.com.voter.VoterApp;
import focuspoint.com.voter.presenters.MainPresenterImpl;
import focuspoint.com.voter.presenters.interfaces.IMainPresenter;

/**
 * Created by root on 06.03.17.
 */

@Module
public class AppModule {
    VoterApp mApplication;

    public AppModule(VoterApp application) {
        mApplication = application;
    }

    @Singleton
    @Provides
    public VoterApp providesApplication(){
        return mApplication;
    }

    @Singleton
    @Provides
    public IMainPresenter privideMainPresenter(){
        return new MainPresenterImpl();
    }



}
