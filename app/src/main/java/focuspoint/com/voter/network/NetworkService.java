package focuspoint.com.voter.network;

import java.util.List;

import focuspoint.com.voter.models.Vote;
import focuspoint.com.voter.models.responseModels.UsersResponseModel;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Interface for Retrofit
 */

public interface NetworkService {
    String VOTES_API_URL = "http://cv83888.tmweb.ru/";


    /**Obtain list of votes from server*/
    @GET("method/votes.php")
    Observable<List<Vote>> getVotes (

    );



}
