package focuspoint.com.voter.presenters.interfaces;

/**
 * Base presenter interface
 */


public interface IPresenter<V> {
    void attachView(V view);

    void detachView();
}


