package focuspoint.com.voter.presenters.interfaces;

import focuspoint.com.voter.views.IMainView;

/**
 * Created by root on 10.03.17.
 */

public interface IMainPresenter extends IPresenter<IMainView> {

    void searchUser (String userName);

}