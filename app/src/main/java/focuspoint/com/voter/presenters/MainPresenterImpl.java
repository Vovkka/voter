package focuspoint.com.voter.presenters;

import focuspoint.com.voter.presenters.interfaces.IMainPresenter;
import focuspoint.com.voter.views.IMainView;

/**
 * Presenter implementation for main screen
 */

public class MainPresenterImpl implements IMainPresenter{
    private IMainView mMainView;

    @Override
    public void searchUser(String userName) {

    }

    @Override
    public void attachView(IMainView view) {
        mMainView = view;
    }

    @Override
    public void detachView() {
        mMainView = null;
    }
}
